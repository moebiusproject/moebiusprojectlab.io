= Moebius Project

//
// Project page only content.
//
ifdef::env-github,env-gitlab[]
This is the repository where the http://moebiusproject.gitlab.io[Moebius Project
website] is stored.

The website is built through a simple Makefile that generates HTML from Asciidoc
files thanks to the awesome https://asciidoctor.org[Asciidoctor].

The Moebius Project is comprised of two parts:

* https://gitlab.com/moebiusproject/moebiustoolkit[Moebius Toolkit]: an
  application about the Infinity Engine game family. See the website or project
  page for details.
* The Moebius Project website (what this repository is about). A site to support
  the application with documentation, screenshots, a showcase, etc., but also a
  place to write about the Baldur's Gate games, like spell guides, reviews,
  links, etc.
endif::[]


//
// Website only content.
//
ifndef::env-github,env-gitlab[]

== What this website is about

This website, still on its infancy, is a small collection of pages devoted to
the Baldur's Gate 1 and 2 games (the ones based on the Infinity Engine). It is
an assorted list of guides, tables and charts that explore very thoroughly some
aspects of the game mechanics.

The writing is hopefully useful to both newcomers and experienced players alike,
but some pages are more useful for power-gamers, as new players can easily be
lost in the complexity of the rules. Inexperienced people might very likely find
answers to problems or questions in specific pages or sections here, while
veteran players will probably enjoy a more in depth read.

== Pages and sections

* <<moebius_toolkit#,Moebius Toolkit>>. The home page of the app made by the
  Moebius Project.
* <<damage_comparisons#,Damage dealing comparisons>>. Explained charts done
  with the Damage Calculator.
* <<arcane_spells#,Arcane spells guide>>. A guide to the arcane spells (Mages,
  Sorcerers and Bards).
* <<divine_spells#,Divine spells guide>>. A guide to the divine spells (Clerics,
  Druids, Shamans, Paladins and Rangers)
* <<compared_spells#,Compared spells>>. A look at different spells that serve
  similar, if not idential purpose, but work in a different way, or at a
  different level.
* <<death_spell_gallery#,Death Spell Gallery>>. A collection of screenshots that
  I took while having fun with Death Spell.
* <<moebius_toolkit#repeated_probability,Repeated probability>>. A chart
  displaying how the chances of something happening at least once vary if you
  apply the effect repeated times. This can be useful to estimate the chance of
  succeeding at affecting an enemy when you hit it multiple times with some
  weapon that carries some special effect (like poison) or you use an spell
  sequencer to apply the same spell two or three times in a row.
* <<mechanics#,Mechanics>>. An in depth explanation of some game mechanics.
  Covers both mechanics and rules obviously relevant to the player, casual or
  experienced, or more subtle calculations (like the rounding performed in some
  cases), for people obsessed with the details.
* <<mivsan_nt_playthrough#,Mivsan_NT's playthrough notes>>. This Baldur's Gate
  saga playthrough is so good, that I had to pay homage to Mivsan_NT and his
  work by indexing the episodes and transcribing some remarkable points of each
  episode. This is a great playthrough to learn a lot about the game, and this
  page is a collection of notes that could be useful in obtaining maximum profit
  from the videos.



== History of the project

=== The initial post

In September 2019
https://www.reddit.com/r/baldursgate/comments/ao55pp/i_have_created_a_damage_calculator_application/[
I told the world for the first time] of a certain project that I just called
Damage Calculator. I did not make the application strictly public, as I only had
the source code to give to others (no Windows or Mac installers, not resources
to make them), and an experimental build as a web page using WebAssembly
(nothing being mature at all). I asked people to message me if they wanted
the URL of the web build, so they can try it, and I think I gave it to a total
of 3 people. I did not get much feedback in the Reddit post either, but I got a
lot of positive votes, which was good enough at the time.

Then in the next months I found that people asked "is it X better than
Y?" more often that I though, and I did myself manually those damage
comparisons, and posted screenshots of the plots. I saw that it was a good way
to show the usefulness of the application, and that I would benefit from making
it public. I kept working slowly on it, adding small features with the plan of
achieving that eventually.

=== The ambitious plan

Time passed, and I eventually got a bit frustrated with some tools. I wasn't
able to make EEKeeper work on Linux (it's a Windows only application, but it
could work through Wine). I knew that a great feature for the Damage Calculator
would be to get information from saved games, or from the item files. I
completely discarded that idea when I started, but I also asked myself the
deadly "but how hard could it be?". Additionally, I wanted to polish the
internals of the Damage Calculator, and add new kind of charts, calculators,
helpers... So a new opportunity to start a hobby project came.

I started a new project repository, and starting getting parsers for the first
Infinity Engine data structures, and some unit tests. I also started creating a
shell that could host the former code of the Damage Calculator, and added a few
features.

I called this Moebius Toolkit, since it's a set of different tools for Infinity
Engine games (and good names using the word "infinity" are mostly taken, so I
went for the name of the creator of the
https://en.wikipedia.org/wiki/M%C3%B6bius_strip[famous strip], which is so tied
visually and functionally with the concept of infinity).


endif::[]
