:AoE: Area of Effect
:CT: Casting Time
:IWDification: https://gibberlings3.github.io/Documentation/readmes/readme-iwdification.html[IWDification]
:OPL: 1 per level
:ORL: 1 round per level
:OTL: 1 turn per level
:ST: Saving Throw
:SCS: https://gibberlings3.github.io/Documentation/readmes/readme-stratagems.html[Sword Coast Stratagems]
:VRC: Visual range of the caster
