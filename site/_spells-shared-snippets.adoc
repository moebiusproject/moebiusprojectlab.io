// tag::scs-added-iwd[]
NOTE: This is an Icewind Dale spell.
It can be brought to Baldur's Gate games with popular mods like {IWDification}
and {SCS} (but modding the game is rarely recommended for a new player!).
The spell might have differences between Icewind Dale and Baldur's Gate (like
the level cap), but significant ones are mentioned in the text.
// end::scs-added-iwd[]

// tag::scs-changed[]
NOTE: This spell description is not from in the vanilla game, is modified by
{SCS} in some form.
// end::scs-changed[]

// tag::tip-created-weapon[]
TIP: This spell is subject to the exploit of created weapons.
It grants no penalties at all when dual wielding, even without any proficiency.
See the <<mechanics#created_weapons,created weapons exploit>> for details.
// end::tip-created-weapon[]

// tag::description-mistakes[]
WARNING: This spell description in game might have mistakes with respect range,
duration, etc., compared to the results in game.
// end::description-mistakes[]

