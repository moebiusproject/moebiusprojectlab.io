= Compared spells
include::_global-attributes.adoc[]
include::_spells-shared-attributes.adoc[]

Many spells compete with others in functionality, so writing about those in
isolation is not enough. Their merits stand not only on their own, but compared
against the fact that others could be used instead.

Choosing which spells to memorize before a rest is an interesting (and sometimes
frustrating) exercise. Choosing spells after level up as a Sorcerer it is even
more. Sorcerer's limited choices can give serious headaches. This page is a
collection of comparisons that were first quickly scratched to think about the
choices for a solo-Sorcerer playthrough, then tidied up for a more general
reading.

TIP: Sometimes the point of discussion one spell against the other might be
moot, if the compared spells belong to different schools, and one specialist
mage might have advantage using one, but not the other, or just might not have
access at all at one or some of the spells. The comparisons hardly address the
case of specialist mages, given that there are too many cases.




== Divine buffs

// |Druid/Ranger/Shaman  |✓         |          |          |          |
|===
|Effect/Characteristic|Bless     |Aid       |Chant     |Prayer    |Recitation

|Level                |1         |2         |2         |3         |4
|Casting time         |9         |5         |9         |6         |7

|Range                |40 ft     |Touch     |0         |0         |0
|Area of effect       |15 ft     |1 creature|15 ft     |30 ft     |25 ft
|Duration (rounds)    |6         |1 + {OPL} |10        |{OPL}     |{OPL}

|THAC0 bonus          |1         |1         |0         |1         |2
|Damage bonus         |1         |1         |0         |1         |0
|Saving throws bonus  |0         |1         |1         |1         |2
|Luck bonus/penalty   |0         |0         |1         |0         |0
|Morale bonus         |5         |0         |0         |0         |0

// Bonus and penalty to enemies, right? Review if Prayer/Recitation do 2 things
|===

// TODO: The bonus to luck, IIRC, is de facto +1 to hit, but not +1 to damage.
// So clarify that.




== Sequencers and triggers

// TODO/FIXME: Claims that the sequencers might not work as I have assumed so.
// I'm afraid it's gonna be hard to get to the end of it, but kjeron seems to
// have it well covered.
// https://forums.beamdog.com/discussion/66870/sequencers-vs-saving-throws
// https://forums.beamdog.com/discussion/78315/sequencers-monsters-not-saving-twice

=== Introduction

<<arcane_spells#minor_sequencer,Minor Sequencer>>,
<<arcane_spells#spell_sequencer,Spell Sequencer>> and
<<arcane_spells#spell_trigger,Spell Trigger>> are 3 spells that serve the same
purposes and work the same way, so the bulk of their section in the guide is
covered here.

NOTE: For simplicity, in the next paragraphs only the word "sequencer" is used,
but it applies to all three above-mentioned spells, including Spell Trigger.

Once one of them is cast (which in almost all occasions will be out of combat),
the game pauses, and a dialog appears. The player is then given the choice of
selecting several spells. Once the dialog is completed, the game resumes as
usual, and the character has gained one use of an special ability, available in
the standard location on the bottom right button. The spells chosen in the
dialog are now instantaneously cast (no longer available till resting), but
stored in the sequencer, like those would be saved in an item, or a scroll.

The player can now rest, optionally changing the spell selection partially or
completely (that includes both the sequencer, and the spells stored in it). That
effectively **expands the number of spells you can cast in a battle** by the
capacity of the sequencer.

Additionally, and probably even more importantly, **now the character can
unleash several spells in the same round, almost instantly, and without risk of
interruption**. If a character is affected by spell failure (Deafness, Miscast
Magic), silence, poisoned, or any other issue that could cause a spell to fail,
it can safely use the sequencer to still unleash some spells.

=== Features and limitations

* Both arcane and divine spells can be stored in a sequencer (for Cleric/Mage
  dual or multiclassed characters)
* Only up to 2 or 3 spells can be stored, and those have to be only up to
  certain level (depends on the sequencer, see the table below).
* The spells have to be memorized and not consumed.
* Most circumstances that disable casting spells, like
  <<arcane_spells#polimorph_self,Polymorph Self>>,
  <<arcane_spells#shapechange,Shapechange>>, still allow the use of sequencers.
// TODO: Seems Tenser's does not. Confirm.
// <<arcane_spells#tensers_transformation,Tenser's Transformation>>
* Many game versions have bugs that cause that a killed and resurrected
  character still has a non-working sequencer active. It is necessary to
  manually use it, see it fail, and prepare it again as usual.
* Combining spells that have different ranges and/or conflicting areas of effect
  is prone to causing one or more of the spells to fail when the sequencer is
  used at a wrong point. Conflicting areas of effect means that some spells can
  only be cast on a target, some only in the caster, and some in any point in
  space.
// TODO: add confirmed examples. The BG wiki has one in the comments: Web & Glitterdust


|===
|Effect                     |Minor Sequencer |Spell Sequencer |Spell Trigger

|Level                      |4               |7               |8
|Stores that many spells    |2               |3               |3
|Stores spells up to level  |2               |4               |6
// |Casting time               |9               |9               |9

|===

=== General approaches for choosing sequenced spells

Just casting two or three random spells at once via the sequencer is good, but
doing so with a specific intention is even more useful.

Since the compared sequencers support a different set of spells (because of the
different supported levels) more detailed examples with explanations are
provided in the description of each individual spell, but those usually fall
into this categories:


Multiply an effect::
--
Doubling or tripling the effect (typically damage) of one spell by using
several all at once. Examples:

* 2 × Magic Missile
* 3 × Skull Trap
* 3 × Lower Resistance

Whether the goal of the spell is doing damage (very common) or causing some
penalty to the opponent (rather uncommon), doing it multiple times in one go is
useful to neutralize quickly one opponent and move on to the next. In the late
game, specially with the <<mods#scs,SCS mod>>, it might also be interesting to
chain several spells that remove protections, when the opponents have many that
need to be stripped.
--

Improve the success chance::
--
Increasing the chance of a spell that allows a save to negate the effect to
succeed at least once. Examples:

* 2 × Blindness
* 2 × Web
* 3 × Remove Magic

When the spell grants the opponent a saving throw to entirely be free of its
effects the purpose of the sequencer is increasing the chance of success in the
first round. While it may seem more optimal to cast the spell over and over only
the necessary amount of times, sometimes succeeding one round earlier can change
the tide of the battle. If you want to reason about how much of an improvement
is applying the same effect twice (or more), check the
<<moebius_toolkit#repeated_probability,repeated probability page>> for a chart
and discussion.
--

Effect variety::
--
Cast a wider net by combining different attacks to ensure that the creatures are
not immune to all of them at once. Examples:

* Web + Stinking Cloud
* Grease + Glitterdust + Slow
* Fireball + Skull Trap + Ice Storm.

If you plan to face a group of mixed opponents with different immunities, or if
you want to be prepared for something you don't expect, maybe you'll need to
have different spells for the situation. A group of undead will be immune to
Stinking Cloud, and one of spiders to Web, but none of those is immune to both.
--

Reapplying protections::
--
Combining defensive or protective spells to reapply quickly if you get stripped
from them. Examples:

* Mirror Image + Invisibility
* Shield + Blur
* Stoneskin + Protection from Magical Weapons + Improved Invisibility.

Besides the ambushes and the unexpected battles (specially for new players), one
typically enters a battle with some spells already in effect. But those expire,
or can be taken down by enemy casters, and you often refresh them during the
battle. Refreshing several at once is obviously better, as it takes less rounds.
You may need to rebuild protections if all of them got stripped at some point of
a long battle. Or if you used your initial round(s) with offensive spells and
stripping opponents' defenses so your fighters do their work, but the opponents
did similarly and now you need to apply new defenses quickly to survive.
--




== Armor versus Shield

// 1st column: left align, proportional width of 4.
// 2 next ones: default width of 1 implicitly.
[cols="<4, 2*"]
|===
|Effect                                                         |Armor|Shield

|Base Armor Class (lower is better)                             |6    |4
|Armor Class modifier against missile weapons (lower is better) |0    |-2
|Protects against Magic Missile                                 |     |✓
|Protects against Mordenkainen's Force Missiles (IWD or mod)    |     |✓
|Casting time (lower is better)                                 |1    |9
|Duration (in game hours)                                       |9    |1
|===

// TODO: improve this note once we have a place to link for IWD spells mods.
// TODO: DRY (the note is copy-pasted through this doc).
// Should it use the snippet from _spells-shared-snippets.adoc?
// I maybe need some trickery to replce "this spell" (snippet) with
// "Mordenkainen's ..." (this context)
// IMPORTANT: note that Sunscorch below suffers the same editorial problem.
NOTE: <<arcane_spells#mordenkainens_force_missiles,Mordenkainen's Force
Missiles>> is an Icewind Dale spell, but can be brought to Baldur's Gate and
Shadows of Amn with some mods.

This two spells are two obvious competitors, as they fulfill a similar purpose
and both are level 1. That makes appear often in questions from newcomers,
specially when looking at picks for Sorcerer spells.

As you can see, <<arcane_spells#shield,Shield>> is better than
<<arcane_spells#armor,Armor>> in every regard, except the duration, where it
loses hands down.
Armor's 9 hours are extremely good compared to just 1 hour for Shield (which is
also fairly good).
But it's the difference in duration that important?
Most often no, but that still depends on play styles and preferences.

A possible use case of Armor is to make a character cast it, go to rest to
refresh spells, and still have 1 hour of duration left to enter the battle.
So for the very early game, where you have very few casts per day and few
alternative resources, it can have more use than Shield.
This approach has been explained and showcased in a no-reload play through with
a solo Sorcerer, so even if it might not be everyone's cup of tea it has been
proven useful in practice for some player.
Don't underestimate it too quickly.

Another possible reason to not pick up Shield in favor of Armor as a
Sorcerer pick, or as a memorized spell as a Mage/Bard, is that there are amulets
that provide exactly the same effect, making it kind of a moot point: a Sorcerer
could pick Armor as normal spell, and use the amulet. See the
https://baldursgate.fandom.com/wiki/Shield_Amulet[Shield Amulet] if you are
interested in that path. Note that this amulet can be used by anyone, which
makes it extremely useful, specially for Monks, Kensais, or anyone who cannot
equip armor, so party composition affects this route. Also note that it uses
charges, so it can be shared, but only if used moderately or recharging often.

However, for all other use cases (most of them, in fact), I think Shield is the
right choice, particularly once one has progressed a bit through the game (but
you can't change spell picks a Sorcerer, so be aware).

Magic Missile immunity is very valuable given how prominent that spell is.
That can very well save your life in multiple ocasions, starting with Tarnesh
(from the Friendly Arm Inn ambush, one of the first big challenges of the saga).
Opponents can still surely hit you with some other damaging spells, but having
out of the equation the most useful one from the first level makes it very
valuable.
It's one of the reasons why Mages in SCS are notoriously tough: they will very
often use Shield, taking an important tool out of your arsenal.
And remember that several traps throw Magic Missile at you as well, so
it's a useful tool if you need to run over traps because you lack a Thief.

The 4 base AC provided by Shield is a bit of help even when you are equipping
some robes that only give AC 5, specially since the -2 AC improvement for
missile weapons applies even when your AC is lower than the 4 AC provided by
Shield, and stacks with every other modifier. Ranged weapons are very dangerous
in BG1, specially against casters, so every bit counts.

Shield will keep being useful in BG2 for large parts of the game as well,
because it will surely take a while to get equipment that improves over a base
AC of 4.

In short: prefer Shield, unless you really know what you are doing.


== Blindness-causing spells

// This are really ugly footnotes that bring to the bottom of the page. I would
// want something like callouts for the table, which are shown just after the
// block in which they are referenced.
// :fn1: footnote:[SCS changes this spell to be single target and have no area of effect.]
// :fn2: footnote:[Only for the blindness-causing effect]

[cols="7a*"]
|===
|Spell                |Level|Duration |{CT}|{AoE}            |{ST}    |Excluded

|Blindness            |1    |2 hours  |2   |1 creature       |Yes     |Necromancers
|Sunscorch            |1    |3 rounds |4   |1 creature       |Yes     |Clerics
|Glitterdust          |2    |4 rounds |2   |20 ft radius     |Yes     |Diviners
|Nature's Beauty      |7    |Permanent|6   |15 ft radius     |No      |Clerics
|Power Word, Blind    |8    |6 rounds |1   |5 ft radius      |No      |Diviners, Bards

|===

// TODO: improve this note once we have a place to link for IWD spells mods.
// TODO: DRY (the note is copy-pasted through this doc).
NOTE: <<divine_spells#sunscorch,Sunscorch>> is an Icewind Dale spell, but can be
brought to Baldur's Gate and Shadows of Amn with some mods.

NOTE: <<divine_spells#natures_beauty,Nature's Beauty>> doesn't allow a saving
throw for the blindness-causing effect, but it has an instant death effect which
of course *does* allow a saving throw.

NOTE: SCS changes <<arcane_spells#power_word_blind,Power Word, Blind>> to be
single target and have no area of effect.

The above table summarizes the spells that can cause the blindness effect. See
<<mechanics#blindness,the blindness section of the game mechanics>>.




== Death Spell versus Death Fog

|===
|Effect                                               |Death Spell |Death Fog

|Kills enemies with 8 HD                              |✓           |
|Kills enemy summons                                  |✓           |✓
|Ignores friendly summons                             |✓           |
|Deals damage to survivors (8 acid points per round)  |            |✓
|Duration over 10 rounds                              |            |✓
|Affects liches (because acid, and is L6)             |            |✓

|===

Both spells are really good. <<arcane_spells#death_spell,Death Spell>> is much,
much easier to use, given that it won't hurt you, and will not send your summons
into oblivion as well. You can have so much fun that I could not resist taking
screenshots of some very satisfying use of it, which I assembled in a
<<death_spell_gallery#,Death Spell Gallery>>.

<<arcane_spells#death_fog,Death Fog>> on the other hand is probably a tiny bit
more powerful, but harder to use. It can fulfill more or less the same role of
Death Spell and Cloudkill combined, but without the instant killing of lesser,
annoying creatures like Umber Hulks or Trolls.

Doing 8 hit points is not a enough to kill anyone, so you will have to *try* to
affect them with the cloud during the 10 rounds of duration to make a
significant damage to opponents, given that they will attempt to move out of the
area of effect, unless you root them to the spot somehow, which I typically do
by keeping them busy with summons (so it will not work here) or just remaining
on the area yourself, suffering that damage or requiring a protection.

But it is a good tool to have to interrupt casters. Is specially useful
considering that Liches are immune to poison and level 5 spells, so Cloudkill is
useless. One point in favor of Death Fog is that there are very few
damage-dealing spells in Levels 5 and 6 that can compete against the "must have"
ones. This is of course arguable: is not that Sunfire is out for a solo
Sorcerer, is that the other spells on Level 5 are very important and without
alternative (like Breach, Spell Immunity, etc.).

However, for a solo Sorcerer, a close alternative with similar effects to Death
Fog is Cloudkill, and there are Wands of Cloudkill, so you should probably stick
with that, and get the instant slaying of any 8 HD creatures from Death Spell,
which has no similar alternative.

If anybody on your party can equip helmets, remember that the
https://baldursgate.fandom.com/wiki/Skull_of_Death[Skull of Death] can cast
Death Spell once per day.




== Protection from weapons

There are a few spells which grant 100% immunity to some group of weapons for a
fixed amount of time. These are different than the well known Mirror Image and
Stoneskin, because this last two can be consumed if hit many times, and don't
grant full immunity (Mirror Image offers a random chance, Stoneskin doesn't
protect against elemental damage, and none provide full protection against
effects). The ones compared here protect from *all the hits, damages and
effects* for the whole duration of the spell, unless the spell is completely
cancelled by Breach or a successful Dispel Magic or Remove Magic.

The main difference between them is in the enchantment level that they grant
protection to. Some may protect, for example, against all the weapons of +3
enchantment or less, while others protect against more powerful ones.

The <<mods#scs,Sword Coast Stratagems>> (SCS) mod has a component to improve
some of them, giving one extra enchantment level of protection. This is
shown in the table below by the second check mark (✓). So if you play with
the default rules, look only at cells with two check marks, and if you play with
that SCS component, look for both cells with one or two checks.

// 1st column: left align, proportional width of 4.
// 8 next ones: center align (default proportional width of 1 implicitly)
[cols="<4, 8*^"]
|===
|Name (level)                           |+0|+1|+2|+3|+4|+5|+6|&ge; +7
|Protection from Normal Weapons (5)     |✓✓|  |  |  |  |  |  |
|Protection from Magical Weapons (6)    |  |✓✓|✓✓|✓✓|✓✓|✓✓|✓✓|✓✓
|Mantle (7)                             |✓✓|✓✓|✓✓| ✓|  |  |  |
|Improved Mantle (8)                    |✓✓|✓✓|✓✓|✓✓| ✓|  |  |
|Absolute Immunity (9)                  |✓✓|✓✓|✓✓|✓✓|✓✓|✓✓| ✓|
|===

////
An idea to make spells be as worth as their level (labelled "A"), so a PfMW is
not the more powerful spell anymore. It would only protect against the weapons
of enchantment equal to 1, or normal weapons:

|===
|Name (level)                           |+0 |+1 |+2 |+3 |+4 |+5 |+6 |&ge; +7
|Protection from Normal Weapons (5)     |✓✓A|   |   |   |   |   |   |
|Protection from Magical Weapons (6)    |  A|✓✓A|✓✓ |✓✓ |✓✓ |✓✓ |✓✓ |✓✓
|Mantle (7)                             |✓✓A|✓✓A|✓✓A| ✓ |   |   |   |
|Improved Mantle (8)                    |✓✓A|✓✓A|✓✓A|✓✓A| ✓ |   |   |
|Absolute Immunity (9)                  |✓✓A|✓✓A|✓✓A|✓✓A|✓✓A|✓✓A| ✓ |
|===

Could be implemented with the hints in:

https://forums.beamdog.com/discussion/comment/1176537#Comment_1176537
////


As you can see, Protection from Magical Weapons is the most powerful one, as
it's from a lower level, and protects against all sort of magical weapons, even
those of the highest enchantment levels. It only doesn't protect against the
less dangerous weapons, the conventional ones. It's a Lich favorite, given that
they are permanently immune to non magical weapons anyway, which makes them
effectively immune to absolutely everything that can be used to hit. In Shadows
of Amn, some plot paths and equipment can make a character of the party immune
to non magical weapons as well. Note that is not common for an opponent to just
carry around a non enchanted weapon "just in case", but it might be a wise
choice for a player.

Note that (except for Protection from Normal Weapons), the duration is fixed to
4 rounds, and doesn't scale with levels.

Mantle, Improved Mantle and Absolute Immunity are rarely worth using except in
some circumstances:

. If your level 6 slots are too packed with spells without alternative, while
  can sacrifice something higher level instead that is not critical.

. If you have them in a spare scroll that you don't need to use for
  transcribing to the spell book. For example, if your character is a Bard
  (possibly solo) that can't memorize any spell of those levels, and hence the
  spell in scroll form is still useful to cast it without preparation. Or if you
  want some Thief with the Use Any Item ability to be buffed by it.


See also:

* <<arcane_spells#protection_from_normal_weapons,Protection from Normal Weapons>>
* <<arcane_spells#protection_from_magical_weapons,Protection from Magical Weapons>>
* <<arcane_spells#mantle,Mantle>>
* <<arcane_spells#improved_mantle,Improved Mantle>>
* <<arcane_spells#absolute_immunity,Absolute Immunity>>


