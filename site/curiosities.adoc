= Curiosities about Infinity Engine games


== Baldurdash made fixes, but also questionable changes[[baldurdash]]

Through the course of <<mivsan_nt_playthrough#,Mivsan_NT's playthrough>> of the
Baldur's Gate trilogy, there were a couple of times where he seemed remember
some line of the conversation with an NPC, or some reference in a note, that
could explain some obscure things.
One example is the Ankheg Plate Mail armor which is fairly hidden in Nashkel's
farming land.
Turns out that a few people have explained that it was the Baldurdash mod which
brought those lines as a "fix" or enhancement to the game.


[quote,YouTube comment,https://www.youtube.com/watch?v=tSjt8Hlpe6g&lc=Ugyj7zxFIxA33OYb5mh4AaABAg]
That conversation about Ankheg Armor (along with some other conversations
providing clues about hidden items) was added by one of Baldurdash vanilla mods,
TotSC Game Text Update I believe. I've never found a version compatible with
BG:EE.


[quote,CamDawg,https://discord.com/channels/155707640255741952/538120099299852299/868989301306835034]
As I learned when making the BG2 Fixpack, Baldurdash had a... loose...
definition of what I would consider a bug.
Dudleyville was even more loose.
You know how The Twisted Rune was connected to the Guarded Compound?
All of that is a "fix" from Baldurdash that connected the GC's rune stone to the
Twisted Rune.
For BG, a similar example--it adds some text to hint at the various hidden items
such as the ankheg armor, tree diamond, and wizard ring.


[quote,CamDawg,https://discord.com/channels/155707640255741952/155707640255741952/898616256532455436]
Also: there's not really a lot to suggest the connection between the Guarded
Compound and the [Twisted Rune]. Most of what people think is the connection was
added content from Baldurdash, which sought to explicitly link them.


[quote, Camdawg, https://discord.com/channels/205226905870270466/531043595252137985/1114563674955325551]
____
[ 4:38 PM ] CamDawg_G3 : While it's been years since I've actually played with
Baldurdash, if the oBG fixes are on par with oBG2, you're better with Sasha's
stuff.
[ 4:38 PM ] CamDawg_G3 : Kevin knew how to fix bugs, but I quibble with his
definition of what a bug is. It's one of the reasons why BG2FP was so
anal-retentive about it.
[ 4:41 PM ] Marco : did he lean towards the 'Irenicus using Ogre voice is not a
bug' side or the other way?
[ 4:42 PM ] CamDawg_G3 : An example: in oBG, he's the one who added text that
implied the suicidal noble outside of Candlekeep hid the diamond in the tree.
[ 4:42 PM ] Marco : huh
[ 4:42 PM ] CamDawg_G3 : He also added text suggesting connections between the
Guarded Compound and the Twisted Rune in BG2.
[ 4:43 PM ] Marco : that's very... creative 😄
[ 4:43 PM ] Marco : yea, that's more of a mod than a bugfix
[ 4:43 PM ] CamDawg_G3 : So it's fixes with some shades of UB, presented as
fixes.
____




== "Did you know?" on Beamdog's forums

https://forums.beamdog.com/discussion/comment/1156240/#Comment_1156240

[quote, Tresset]
____
Godbow is not an exploit. It is a tool, made by Beamdog, for testing purposes.
It was never intended to be used in an actual, non-test run, playthrough.
____



[quote, Elminster]
____
Relevant tweet

Trent Oster Dec 1, 2012
"We used killswords back in BG2 for testing speed runs. Godbows are way cooler
in #BGEE . #EnemiesGoBoom"

https://mobile.twitter.com/trentoster/status/274764797986369536
____


'''

Sanctuary has some issues, that 2.5 fixes, and maybe even future releases. The
first post mentions a way to use healing while under Sanctuary.

https://forums.beamdog.com/discussion/comment/1157272/#Comment_1157272

[quote, JuliusBorisov]
____
Yeah, the 2.5 patch introduced that improvement. There is hope 2.6 will fix the remaining bugs with Sanctuary.

https://forums.beamdog.com/discussion/74427/odd-moments-bugs-with-some-bg1-2-ee-clerical-spells
https://forums.beamdog.com/discussion/74448/spells-that-wrongly-break-sanctuary
____


'''

https://forums.beamdog.com/discussion/comment/1158939/#Comment_1158939

[quote, Semiticgoddess]
____
As a tiefling, Haer'dalis has a +1 racial THAC0 bonus to short swords, long
swords, and bows, just like an elf.
____


[quote, jmerry]
____
And the other half of this - ever wonder why Haer'Dalis gets specialization in
short sword and long sword (the latter after you put in a proficiency point)?
He's flavored as being part of Planescape's Doomguard faction. They get +2 to
hit and damage with short swords and long swords. Combine the +1/+2 from
specialization with the +1 racial bonus, and that's right on.

The bonus to bows... no good flavor explanation for that.

I just checked his script - there's a second condition as well. If he has at
least 1.2 million XP and one proficiency point in longsword, he gets the second
for free.
____



