= Damage dealing comparisons
:toc:
:sectanchors:
:sectnums:
:idprefix:
:IMG: damage_comparisons
:DATA: assets/damage_comparisons

A bunch of comparisons of several damage dealing arrangements done with the
<<moebius_toolkit#damage_calculator,Damage Calculator>> module of Moebius
Toolkit.

NOTE: This page is not yet very complete. It contains some damage comparisons
that I've done for myself, and the ones done at user request. For now, you can
also check the https://suy.gitlab.io/damage-calculator/#_example_comparisons[
example damage comparisons of the initial Damage Calculator post]. It is very
through and covers some great classic heated debates (like Monks). Although I
would do those differently today, with the gained experience, so take it with a
pinch of salt. Also note that at that point, the resistance to damage wasn't
properly calculated, so those cases are not correct.



== One character comparisons (with different weapons or spells)


=== Buffing a mid level Khalid

Should we increase performance with Haste or Strength at low/mid levels?

In this comparison we see Khalid at level 5, without any gear or spell to boost
his damage, versus when his Strength is raised to 19 (+3 to hit, +7 to
damage) and when he is hasted (increases from 2.5 to 3 attacks per round). The
comparison would apply the same to any unkitted Fighter of the same level and
base Strength. He would have the Mastery in Long Swords, and would be wielding
Varscona +2 (1 extra point of cold damage).

image::damage_comparisons/khalid-level-5.png[]

link:assets/damage_comparisons/khalid-level-5.json[Download comparison file].


=== Flame Blade

See the <<divine_spells#flame_blade,Flame Blade>> spell. It conjures a
non-magical sword that deals meager physical damage, but quite respectable fire
damage on top. Here it is shown in comparison with other weapons. The comparison
is made with a chracter like Branwen in mind, who doesn't have any bonuses from
Strength, and who has a for free Spiritual Hammer special ability (shown in the
charts).

image::damage_comparisons/branwen-level-7.png[]

If you increased her Strenght to 18 (e.g. via a potion), this would be the
result:

image::damage_comparisons/branwen-level-7-bonuses.png[]

link:assets/damage_comparisons/branwen-level-7.json[Download comparison file]
(without bonuses).
link:assets/damage_comparisons/branwen-level-7-bonuses.json[Download comparison file]
(with bonuses).


=== Kensai using Belm without proficiency for dual wielding or scimitar

This comparisons comes from a question posted on Reddit, asking whether
https://www.reddit.com/r/baldursgate/comments/i9b7jm/kensai_is_it_worth_it_to_put_belm_scrimitar_in/[a
Kensai specialized in Katanas only would benefit from Belm in the off hand].

The poster did not give too much detail about his character, so the following is
assumed: the character is a Kensai at level 9 (+3 to hit and damage) with 19
Strength (+3 to hit, +7 to damage), and has Grand Mastery (5 points) in Katana,
but only one point in Two Weapon Style (-2 to hit in the main hand, -6 in the
off hand). The calculations to compare are dual wielding two Katana +1 versus
changing to Belm in the off-hand, and versus wielding only one Katana +1.

image::{IMG}/kensai-unproficient-in-scimitars-with-and-without-belm.png[]

As you can see, even though the character has no proficiency in dual wielding
at all, all the dual wielding combinations are superior to using only one
weapon.

link:{DATA}/kensai-unproficient-in-scimitars-with-and-without-belm.json[Download
comparison file].


=== Archer vs Drizzt

// https://www.reddit.com/r/baldursgate/comments/ifkl49/baldurs_gate_speedrun_in_summer_games_done_quick/
This comparison was inspired by a player named
https://www.youtube.com/channel/UCoESFfspB-AWhO1BZDJQBsA[Seriously Surly], who
did a https://www.youtube.com/watch?v=oH1tah5P-8w[Legacy of Bhaal speedrun of
BG1], and as a bonus chapter, showed how to fight Drizzt using an Archer and a
Berserker. The first fight was so easy that I had to look at the numbers, as
basically it was just a few hits with Arrows of Detonation. Something wasn't
making proper sense.

Drizzt's numbers are:

- 98% magic resistance, so the special elemental damage only will affect him 2%
  of the times. The Damage Calculator was filled so that said elemental damage
  only happens 2% of the time, but that's so low that we could even ignore it.
  The half damage of the save vs spell is not considered as the Damage
  Calculator doesn't support it at the moment this was done.
- 30% resistance to missile damage.

And the late game Archer (at the end of BG1's XP cap is) would be at level 8,
though Seriously Surly had it overleveled by one, so at level 9. That level is
Siege of Dragonspear or Black Pits cap, so it's not that much experience.
However, that level allows reaching the 5th proficiency point for reaching Grand
Mastery, so take it into account if you care.

The inputs to the calculator are considering:

- An Elf, for +1 to THAC0 with bows.
- A level 8 Archer, with +2 to THAC0 and damage from the kit's bonuses.
- With maximum proficiency available at that level (High Mastery) in the chosen
  ranged weapon.
- Hasted, for reaching 4 attacks per round.
- With 25 Dexterity, to buff THAC0 by +5.
- Equipping Helm of Balduran and Legacy of the Masters for a final +2 to hit and
  damage bonus.

The player on the video shows some other buffs, like drinking Strength potions
to increase damage (which is obviously wrong for ranged attacks), but also
drinking potions of power and heroism, which I've not considered for simplicity.

One thing that I've learnt later, is that Arrows of Detonation are designed such
that they would never fail. If you equip them, you will see a huge number being
subtracted to your THAC0 in the details of the Inventory screen. The game
implements that specific bonus as "never fail", in a similar way to what the
Critical Strike HLA does, but without doubling damage if the opponent is
unprotected to critical hits. We just use the Critical Strike checkbox in the
Damage Calculator, and make the enemy protected against critical hits.


image::{IMG}/archer-vs-drizzt.png[]

link:{DATA}/archer-vs-drizzt.json[Download comparison file] (note that the 30%
damage reduction of the enemy is not loaded from the file: set it manually).


//------------------------------------------------------------------------------
== Character versus different character


=== Fighter vs Paladin (late BG1 and SoD)

At the experience cap of Baldur's Gate, both Paladin and Fighter can reach the
same level, the 8th. However, Fighters can progress in proficiencies beyond the
two initial points, and will end up with four in their preferred weapon (High
master). That makes a difference, but it is not easy to notice. However, at the
end of Siege of Dragonspear (or at the very early Shadows of Amn), with half a
million XP, Fighter can reach Grand Mastery, doing not only more damage per hit,
but also doing more attacks per round.

image::damage_comparisons/fighter-vs-paladin-late-bg1-and-sod.png[]
link:assets/damage_comparisons/fighter-vs-paladin-late-bg1-and-sod.json[Download comparison file].


////
== Firetooth vs Gesen

Asked by Auve in Discord.


== Crushing vs Slashing impact on THAC0

I've noted in the chat with Auve and Nex(?) that THAC0 was relevant. So the
impact of Crushing damage in BG1EE should be relevant even though Ashideena is
less damaging.

== Additional damage by ammonition on weapons that don't require it

https://www.reddit.com/r/baldursgate/comments/g1ntx6/firetooth_sling_of_everard_boomerang_dagger_and/

____
Specifically, if you use regular ammo, many/most ammoless ranged weapons add
their normal damage to the ammo's base damage.

So for gesen:

Gesen normally deals 2 + 1d8 lightning. If you use a quiver of plenty, it deals
1d6 (normal arrow) + 2 + 1d8 lightning (gesen's damage).
____


[quote]
Still +2, but you can always unequip your arrows when you are fighting something
that requires +4.
////
