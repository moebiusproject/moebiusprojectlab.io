= Game mechanics
include::_global-attributes.adoc[]

This page covers different game mechanics in Baldur's Gate and/or the Infinity
Engine games.

// Editorial note to self. I think the order of the sections might be a bit
// confusing, but it follows a certain logic:
// - Mechanic A (alphabetically first)
// - Mechanic B (alphabetically second)
// - ...
// - Miscellaneous (small quirks like rounding up/down, or subtleties that are
//   maybe for very, very curious/technical people, boring for most).
// - Exploits (to the bottom, even when it relates to something on the top, like
//   it happens with blindness, to indicate that it might be seen as cheesy by
//   some people, or that might be patched in a future release or by mods).




== Blindness

Blindness is one of the most powerful effects that the player has to cripple
enemies. Blinded creatures suffer:

* A penalty of -4 to their attack rolls.
* A penalty of -4 to armor class.
* A significantly reduced sight range (to about the range of an average melee
  weapon).

Spells like <<arcane_spells#blindness,Blindness>> only mention the first two
points in the spell description, but the important one is the latter!

An enemy without enough sight range **can't find opponents to target** because
it can only do so on the ones inside said range.
If you blind an opponent while your characters are next to it, make them move a
little bit away in a non straight line, and most likely they will be 100% safe
from the blinded creature.
Attack it with ranged weapons or even with melee weapons with a large range like
Two Handed swords, or a Monk's unarmed attack, and it will not fight back.

If you succeed at blinding a character whose threat is casting spells or
shooting projectiles at you, it will be hardly able to do anything. Even with
mods that enhance the AI, many enemies will just *stand still waiting to be
killed*. At best they will move randomly to have some chance of finding someone
to attack or getting out of your attack range.

But if you get blinded a monster who is only capable of attacking in melee (for
example, spiders or many undead), and you do so while it's not yet in range to
hit someone, then it won't be able to do a single thing. And again, if it's in
range, you can just move away a little bit, and it will be out of the fight as
well.

IMPORTANT: Most, but not all of this applies to the human-controlled party.
The player will be able to direct any blinded party member to attack enemies who
are on the line of sight of *any other* party member, even if the blinded party
member doesn't have that enemy very close (this also happens without blindness
involved).
If your ranged attackers get blinded, they will have to resort to close combat
with severe penalties due to the blindness, and they should not use ranged
weapons against a melee attacker (which adds even worse penalties).
Likewise problematic for a spell caster who wants to cast from the distance
(they will not be able to hurl a Fireball in a safe area of effect).
And if decide to play a solo character, you can't get the benefits of the party
sight range of course.

Blindness is so powerful that mods like <<mods#scs,Sword Coast Stratagems>> make
some improvements to make it more realistic and balanced. One of the solutions
is to make clerics aware of allies who are blinded, and instruct them to use the
divine spell <<divine_spells#cure_disease,Cure Disease>> on the blinded creature
to heal them, and enable them back for the fight (you should consider using this
spell as well).

Another change to the vanilla game done by SCS to make blindness less of a
problem, is to boost a spell like <<arcane_spells#true_sight,True Sight>> (or
the equivalent <<divine_spells#true_seeing,True Seeing>>) with the effect of
restoring sight on the caster. This feature is also available to the player, of
course, so make good note, because your own spell casters will want to attack
with spells using the usual range, specially the ones which are not party
friendly.

Still, even with SCS's changes to the spells and AI, blindness remains one of
the best ways to disable powerful foes that might be immune to anything else. It
might be *the preferred way to do it* as well, because many opponents gain some
immunities via the Berserker kit and their Enrage ability. And yes, the -4
penalties are useful as well in attacking them in melee range if needed.

See the <<compared_spells#blindness_causing_spells,blindness-causing spells>>
for a list of compared spells that cause blindness.

See below the <<#hiding_while_blinded,hiding while blinded exploit>> to learn
about using blindness to your own advantage, and use stealth even when enemies
are around.




== Damage Resistance

Weapons, traps and some spells produce damage. That damage is always of some
kind:

* Fire (normally printed in red).
* Cold (normally printed in light blue).
* Electricity (normally printed in dark blue).
* Acid (normally printed in pale green).
* Poison (normally printed in dark green).
* Magical (normally printed in yellow).
* Physical damage of either the slashing, piercing, crushing or missile type
  (printed in standard white).

image::mechanics_damage_types_colors.png[role=text-center]

Damage Resistance reduces the amount of damage produced when that damage is not
avoided.
For example, the Melf's Acid Arrow spell creates an arrow that never misses its
target, but acid Damage Resistance makes the spell less damaging, or not
damaging at all, as explained below.
Likewise if you are hit with a real weapon when you are held or stunned: the
attack never misses, but if you have physical Damage Resistance, you will be hit
for less damage.

Resistance is not to be confused with other mechanics:

* Armor Class prevents being hit by a physical attack (fully dodging it), hence
  preventing damage and some on hit effects, but it's *not* Damage Resistance.
* Magic Resistance prevents magic having any effect whatsoever, so in a sense is
  similar to Armor Class.
* Saving throws might make a spell do half or no damage, but the save is
  different from Damage Resistance, and both stack.

TIP: Armor Class and Magic Resistance might seem always better than Damage
Resistance, as they avoid the full damage, and other non-damaging effects.
That is more or less true.
But there are enemies which can hit so accurately that only an extremely good AC
will make a difference, and spells which bypass Magic Resistance.
Additionally, there are stages on the saga where it is easy to improve one thing
and not the other, so it's useful to know about both.

When everything fails and a hit or a spell that deals damage succeeds,
resistance to damage can reduce, cancel, or even **revert** (heal) the damage
that a creature is supposed to receive without resistance.
Some examples:

* 50% resistance to a damage type will reduce it in half.
* 100% resistance to a damage type will make one not receive any damage.
* 125% resistance to a damage type will make one immune to it, but also will
  make one regain hit points for a quarter of the damage that would one suffer
  without resistance.

// TODO: damage vulnerability, e.g., Belt of Antipode
// TODO: the odd cap of 127%, and how it increases over 100% (slower than below)

// TODO: damage gets rounded down. Armor of Faith has a proper explanation, so
// maybe try not to duplicate it?

See also:

https://baldursgate.fandom.com/wiki/Damage[Damage in the Fandom wiki].




////
// TODO
// https://pihwiki.bgforge.net/Baldur%27s_Gate:_Ultimate_Pickpocket_Reference

== Open Locks

In order to open a lock, your Open Locks score must be close to the score of the
lock.

////



== Sneak Attack

Sneak Attack is an optional feature that can be enabled in Gameplay Options.
It replaces the Backstab that Thieves and Stalkers can perform.
In IceWind Dale Enhanced Edition the player is able choose from Backstab (the
default) or Sneak Attack, and it can be switched in Gameplay Options.
It can be enabled in Baldur's Gate as well, but it's either manually edited in a
configuration file, or displayed in the Gameplay Options section using a mod.

It has it's pros and cons respect backstab, but it's a full replacement (you
can't have both in the same fight).
The way it is triggered in combat is a bit different from Backstab, as it
doesn't require being invisible or hidden in shadows.
Being unfamiliar with the mechanic myself, I had to do some tests in game to
confirm how it was supposed to work in IWD, as the searches I did were not too
detailed, and often had contradictory information.

The tables with the amount of damage dealt on each Sneak Attack (or the backstab
multiplier) are available in the IWD wiki in the
https://icewinddale.fandom.com/wiki/Thief#Class_Features[Thief] and
https://icewinddale.fandom.com/wiki/Ranger#Stalker[Stalker] pages.

Some additional resources:

* https://forums.beamdog.com/discussion/37339/the-wonders-of-sneak-attack/p1[The
  Wonders of Sneak Attack]. Some tactical discussion on the pros and cons.
* https://forums.beamdog.com/discussion/69726/sneak-attack-in-bg[Sneak Attack in
  BG]. A more technical post on how to enable it in BG, and the special
  treatment that opponents have: they don't need to position, so each attack can
  be a Sneak Attack.




=== Can only happen once per round

https://forums.beamdog.com/discussion/36275/shadowdancer-can-only-sneak-attack-once-per-target-is-it-intended-or-bugged[Shadowdancer
can only Sneak Attack once per target, is it intended or bugged?]

[quote, Avenger]
____
It is intended that everyone backstabbed gets some time of immunity. (This was
in vanilla too).
____

image::mechanics_sneak_attack_ineffective_more_than_once.png[]


=== Can't be performed with any weapon

Only weapons suitable for backstab can be used in an Sneak Attack.
Those are any *melee* weapon available to a single class Thief (without Use Any
Item).
That is sometimes confusing, but here are some reminders:

* No Two-Handed Sword or Bastard Sword or Axe, despite being slashing weapons.
* Quarterstaves or Clubs are fine despite being crushing weapons.
* No Staff of the Magi (which is only available to Thieves via Use Any Item, or
  a second class).

image::mechanics_sneak_attack_unsuitable_weapon.png[]


=== Works if positioned from behind

Getting the positioning correctly is important, so it might be difficult against
moving enemies.

image::mechanics_sneak_attack_fails_from_the_side.png[]
image::mechanics_sneak_attack_from_behind.png[]




== Miscellaneous

This is a list of curiosities found while experimenting and trying things, more
than really important game mechanics.

=== Saving throw for half damage rounds down

Many spells allow a saving throw for half damage, but what happens when halving
a number yields a fraction? One half of 3 is 1.5, but damage can only dealt in
quantities which are natural numbers, so does it get rounded to 2 or 1?

It gets rounded **down**, so 3/2 becomes 1.

Here is an experiment to reproduce it. I made Jaheira, as a level 2 Druid, cast
<<divine_spells#sunscorch,Sunscorch>> at a character with good saving throws.
Sunscorch allows a saving throw to avoid a <<#blindness>> effect, and another
saving throw for taking half damage (if you don't have Sunscorch because you
don't use any mods, try <<arcane_spells#burning_hands,Burning Hands>> instead).
Since the damage is 1d6+2, the lowest one can get is 3, and if the saving throw
is made, half of that. If in one attempt you get a 1, here you go.

image::saving_throw_for_half_damage_rounds_down.png[]




== Exploits

All software has bugs, and games are no exception.
This section covers things that, for most people, are considered bugs or quirks
in the implementation of the Infinity Engine games, not intended ways to play.
That said, the usual common sense rule applies: there is no cheating in a single
player game, play however you find most enjoyable.
Just consider the players who might find some things immersion breaking and
unintended.


=== Created weapons

When you use one of the many spells that create a single-handed magical weapon
in your main hand (from <<divine_spells#shillelagh,Shillelagh>> to
<<arcane_spells#black_blade_of_disaster,Black Blade of Disaster>>), you can
still equip a second weapon in the *other* hand.
The game is subject to a bug where it doesn't take into account how many
proficiency points you have in Two Weapon Style.
You will be able to attack with two weapons, and **none of the two** will have
**any** penalty to hit, compared to what you would have if you were using just
one weapon in the main hand.
This is even better than having the maximum points in Two Weapon Style, as in
the vanilla game three points will give you still a -2 to hit in the second
hand.


=== Hiding while blinded

As explained in the <<#blindness>> section above, blinded characters have a very
reduced sight range.
That stops them from casting spells or attacking far away enemies, as they are
just not visible to the blinded creature.
When a character attempts to activate Stealth, the game engine checks if the
actor who wants to become invisible sees enemies, and if it does, it fails
immediately.
This was very likely done for simplicity and performance reasons: instead of
having to do a sight range check in all the hostile creatures to know if they
see the about to be hidden character, it is done directly in the stealthy one.
This leads to a blindness-related exploit: a character who can use Stealth can
hide *in plain sight of enemies* as long as the character itself is blinded, and
the enemies are not right next to it.
This is more or less the same ability that a Shadowdancer has.
